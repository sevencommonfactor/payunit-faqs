## I have submitted my KYC and it has been verified, yet I cannot subscribe to more than One psp
You haven’t done a 2FA validation. <br> 
If you have not yet logged in, login to your account using your credentials.<br>
![](https://gitlab.com/sevencommonfactor/payunit-faqs/-/raw/e80e268bf537aa3759ec9f637185b4ce1ca4f421/images/payunit_login.png)
Go to profile on your dashboard and scroll to the 2FA validation section and with an MTN or Orange number follow the instructions.
![](https://gitlab.com/sevencommonfactor/payunit-faqs/-/raw/b15374b3bd58a920927e3ce6269b1742313330b6/images/profile_and_2fa.png)