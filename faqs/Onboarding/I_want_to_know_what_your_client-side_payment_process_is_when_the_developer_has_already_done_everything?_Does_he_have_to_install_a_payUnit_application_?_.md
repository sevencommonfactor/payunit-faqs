## I want to know what your client-side payment process is when the developer has already done everything? Does he have to install a payUnit application?
When you have integrated PayUnit within an application, the customer will click on pay with PayUnit he will have the choice of several PSP (which is as a function of what you have subscribed to in your account).<br>
![](https://payunit.net/wp-content/uploads/2021/04/Screenshot-from-2021-04-21-17-37-05-1.png)

In case he takes Orange money, he just enters his number, he receives a notification from orange he validates and he is debited, the payment is made, this money is kept in your PayUnit account.
We know how much was paid, when and at what time your transactions were made, in the end you log on the platform.

You collect your money from the platform by making a cashout request.<br>
![](https://gitlab.com/sevencommonfactor/payunit-faqs/-/raw/167865fe363459961cee15cfdfe1866619dd85c8/images/payunit_cashout_request.png)
The easiest way to understand is to try it. <br>
I hope it is clearer now