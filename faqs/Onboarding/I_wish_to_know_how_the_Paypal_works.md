## I wish to know how the Paypal works
Once a client clicks on the pay button and selects the payment service provider PayPal, the PayPal page opens where the person has a button labeled PayPal.<br> 
The client will click on the button, and a secured window will be opened for the individual to input his/her PayPal credentials and pay via the platform.