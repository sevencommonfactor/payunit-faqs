## How do I create an account on Payunit

Go to [app.payunit.net](https://app.payunit.net) and click on the button ``` Sign Up ```, and follow the instructions.
![](https://gitlab.com/sevencommonfactor/payunit-faqs/-/raw/b54db64a2abddd48c0716a97d47f837853b141f8/images/create_account.png)
