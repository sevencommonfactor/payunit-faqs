## Can this work in Equatorial Guinea???
- It is available only for Cameroon Merchants. But if you have a Cameroon mobile money number, you can use the service. But we are looking forward to extending.
- PayUnit supports PayPal and Card transactions which are irrespective of the country in which you are found. You can implement PayUnit, but you will be limited to using just PayPal and Card payment given that you are not in Cameroon where you can use mobile money.
