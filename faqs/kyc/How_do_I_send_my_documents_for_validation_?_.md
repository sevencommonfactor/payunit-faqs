## How do I send my documents for validation?
Go to the KYC section, select your category, upload the pdf document of maximum 40kb and upload.
![](https://gitlab.com/sevencommonfactor/payunit-faqs/-/raw/e80e268bf537aa3759ec9f637185b4ce1ca4f421/images/kyc_upload.png)