## Do you need some kind of documents? At what stage do you need KYC?
Yes we do. We will need your Personal ID and Business License in order for you to be able to subscribe to more than one payment service provider (psp). <br>

You can submit these documents on your dashboard by clicking on the section KYC and uploading your documents.
![](https://gitlab.com/sevencommonfactor/payunit-faqs/-/raw/e80e268bf537aa3759ec9f637185b4ce1ca4f421/images/kyc_upload.png)