# PHP SDK
<br>

### Pre-requisites

-   PHP = 7.2 && PHP>=7.4+ (Not working with PHP 7.3)
-   Create an account on Paynit as a merchant. Click [to create an account](https://app.payunit.net/#/).
-   Get the merchant Api User, merchant API key, and merchant API Password from the merchant dashboard under the credentials section

<br>

![](https://payunit.net/wp-content/uploads/2021/04/Untitled-2-1.jpg)

<br>
To Get Started with this SDK, Make sure you have a PHP application ready.Also make sure you subscribe to the various payment methods you want

<br>

## Installation

```
composer require sevengps/payunit
```
<br>

## Using the SDK

To conveniently use Payunit, Add the Payunit Namespace in your desired controller.

```
 use SevenGps\PayUnit
```

Create a new instance of the Payunit class and pass in all the required parameters.

```php
$myPayment = new PayUnit(

 "api_key",

 "api_password",

 "api_username",

 "returnUrl",

 "notifyUrl",

 "mode",

 "description",

 "purchaseRef",

 "currency",

 "name",

 "transactionId"

 );
```
<br>
Call the MakePayment method to make a payment

```
 $myPayment->makePayment("total_amount");
```

## Configuration 


-   To Test Visa/Master Card in the Sandbox environment use the following information :
    -   Card Number: **4242 4224 2424 2424** or **2223 0000 4840 0011**

-   To test PayPal in the Sandbox environment use the following credential :
    -   Email: **sb-hf17g4673731@business.example.com**
    -   password**: ehQ5_)dA**

<br>

| Attribute | Description | Mandatory |
|:-------| :--------- | ---------:|
| apiUsername | Merchant Api Username gotten from merchant dashboard under credentials section | yes |
| apiPassword | Merchant Api Password  gotten from merchant dashboard under credentials section | yes |
| api_key | Merchant Api Key gotten from merchant dashboard under credentials section | yes |
| mode | The current mode operation. Can either be "**test**" when testing in sandbox or "**live**" when ready for production. | yes |
| return_url | The url or endpoint to be called upon payment completion | yes |
| notify_url | The url or endpoint to submit a transaction response to.This url or endpoint accepts a **POST** request of format:<br> <br> { <br> "transaction_id:"6465464", <br>"transaction_amount":"5000",<br>"transaction_status":"SUCCESS",<br>"error":null,<br>"message":"Transaction of 5000 XAF was successfully completed"<br>} | no |
| purchaseRef | A reference which you can give from your end to a particular transaction | no |
| total_amount | The amount or price of the product/products to be paid for. | yes |
| description | A description you can give to this type of  transaction | no |
| name | Name of the merchant | yes |
| currency | Can be XAF, USD or any currently supported currency | yes |
| transaction_id | *id that uniquely identifies the transaction and I must be unique for a transaction. This id should be alpha numeric and less than 20 characters* | yes
|

<br>

If everything is put in place, a call to the PayUnit API will be made and if the request turns out successful, you will be redirected to the PayUnit payment interface. The interface is shown below.

![](https://payunit.net/wp-content/uploads/2021/04/Screenshot-from-2021-04-21-17-37-05-1.png)

#### Demo

You can check out this demo at
``` 
https://gitlab.com/sevencommonfactor/payunit-php-demo/-/tree/dev 
```
to see payunit in action.
<br>

Watch demo video here 

[![Watch demo video here](https://i.ytimg.com/vi_webp/9q6ZgDJH23k/maxresdefault.webp)](https://youtu.be/9q6ZgDJH23k "Watch demo video here")
<br>

## Recommendations

Please for security reasons make sure you read your Api key, Api password and Api user from an environment file

