# PayUnit WordPress Plugin
<br>
A WooCommerce payment plugin that uses PayUnit payments aggregator to process merchant payments. Allows users to implement seamless payment flows on their WooCommerce applications to pay for items in a cart.

<br>

[Download the plugin](https://payunit.net/plugin_order/checkout-page/)

Changelog
---------

### = 1.0 =

Initial release

Requires at least: 5.1\
Tested up to: 5.2\
Requires PHP: 7.2

### = 1.1.0 =

-   **Remote Updates**: Notifications about the plugin updates will be constantly sent to you which you can then update directly on your WordPress dashboard by clicking the "Update" button.
-   **Currency Switching**: The plugin now supports 4 currencies; namely: XAF, USD, GBP, and EURO. You can switch within these currencies without your plugin deactivating.
-   Bug fixes.
<br>

## Description


PayUnit for WooCommerce is an elegant plugin built with the merchant in mind and at the very core of its design. It is built to let merchants focus on providing great products on their sites without having to think of how to get customer\'s money to them. Powered on the WooCommerce platform, the PayUnit plugin works out of the box by hosting a great payment experience for users of most regions of Africa and the world. By creating a PayUnit account, you can begin to accept payments with local and international payment providers including VISA/MASTERCARD, Mobile Money, Orange Money, YUP, Express Union, and many more as shall become automatically available on your PayUnit account.

Using PayUnit's innovative and secure plugin, support integration with the WooCommerce subscriptions module is a guarantee. PayUnit meets the highest standards of security and reliability and is PCI Level 1 compliant.

<br>

## For cameroon based merchants


You will need to set up a payout account with PayUnit. This account will allow you to accept payments from anyone in the world using all ma\
jor payment service providers available. You are also advised to carefully read the pricing scheme described [HERE](https://payunit.net/pricing/). There is a processing maximum volume for accounts that are not KYC compliant as shall be indicated on your account dashboard from time to time. Account setup is typically a matter of minutes and if you have the right people on your team, integration should take a day or two before you can start collecting payments out of the box on your website.

<br>

## For merchants outside of cameroon


At this time, because of the complexity of cross-border payment processing, we are only accepting applicants with valid mobile money accounts in which deposits for payments collected can be made. Merchants without such payout accounts shall not be able to withdraw until they have a confirmed mobile money account from any of the operators (MTN or Orange). Please [contact](https://payunit.net/#contact) PayUnit more information.

Key features
------------

-   Seamless integration into the WooCommerce checkout page.
-   Customers are redirected to focus on their payment but return o the site once payment is completed
-   Accept most major credit cards and mobile money payments
-   Prevent Fraud -- Identify suspicious transactions with our value-added products and built-in fraud tools.
-   Risk Management -- Our gateway is PCI Level 1 compliant (geek for secure as hell). All card data is handled by our Card processor. We do not touch your card data as we don\'t even have visibility on that.
-   Receive Payments Quickly -- Your funds are deposited into your payout accounts upon request. May take some 2 to three business days.

Note: This extension requires an SSL Certificate to be installed on your site to ensure your customer's credit card details are safe.

<br>

## Frequently Asked Questions
---
<br>

***How does this work?***

The easiest way to get started with PayUnit payments is by visiting the [website](https://payunit.net).\
Create your account then head to the documentation to add that to your code.

<br>


***Why do I see only few payment providers to subscribe to on my dashboard?***

As we work very hard to add more and more payment providers, those you would see on your dashboard are only those your current account is eligible to signup for. You may signup only to the number of PSPs your account limits allow.

<br>

***What currency do I allow customers to use for payments ?***

For now, all payments to local PSPs are in XAF while all card payments are in USD. This means that card payments may incure a little more cost charges on currency conversion for cards that have their currency in any other than USD. But don\'t worry, it is not going to always be like this and we will be sure to inform you once we make progress to unify payments on the platform.

<br>


***Why does my plugin activation fail ?***

This may be because your currency is not in XAF or the WooCommerce module is not installed and active.

<br>


***Why am I unable to subscribe to more than one PSP?***

In order to benefit from the full power of PayUnit, merchants are expected to submit additional documentation in order to be fully KYC compliant. Account validation may take up to three business days and you shall be able to subscribe to all available PSPs once you account limits have been lifted.\
Answer to foo bar dilemma.

<br>


### Screenshots

1.  PayUnit dashboard credential display
2.  PayUnit plugin successfully installed and activated
3.  PayUnit plugin dashboard in WordPress site
4.  Order checkout info
5.  Payment is redirected to PayUnit central platform for processing
6.  Redirecting to WordPress shop
7.  Payment successful notice
8.  Order transaction WordPress info
9.  Currency settings to in Woocommerce

**Demo**

<br>

[![Watch demo video here](https://i.ytimg.com/vi_webp/l8JlcfsoIvw/maxresdefault.webp)](https://youtu.be/l8JlcfsoIvw "Watch demo video here")

<br>

[Download the plugin](https://payunit.net/plugin_order/checkout-page/)

