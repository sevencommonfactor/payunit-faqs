# Python SDK
<br>

## Pre-requisites

-   Create an account on Payunit as a merchant. Click [to create an account](https://app.payunit.net/#/).
-   Get the merchant Api User, merchant API key, and merchant API Password from the merchant dashboard under the credentials section.

<br>

![](https://payunit.net/wp-content/uploads/2021/04/Untitled-2-3.jpg)

<br>

## Installation

```
pip install payunit
```

### Usage

Now, proceed to creating a payment instance with **payUnit**( ) .

from  payUnit import payUnit

​
```python
from flask import Flask,render_template,request

app = Flask(__name__)

​

payment = payUnit({

 "apiUsername":'',

 "apiPassword":'',

 "api_key":'',

 "return_url": "",

 "notify_url":"",

 "mode": "",

 "name": "",

 "description": "",

 "purchaseRef": "",

 "currency": "",

 "transaction_id":  ""

})

​
# main driver function

if __name__ == '__main__':

 app.run()
```
<br>


That set, payments can now be done by using **makePayment( )** method, with the transaction amount as parameter.

```
 payment.makePayment(5000)
```
<br>

### Configuration

-   To Test Visa/Master Card in the Sandbox environment use the following information :
    -   Card Number: **4242 4224 2424 2424** or **2223 0000 4840 0011**

-   To test PayPal in the Sandbox environment use the following credential :
    -   Email: **sb-hf17g4673731@business.example.com**
    -   password**: ehQ5_)dA**

<br>

| Attribute | Description | Mandatory |
|:-------| :--------- | ---------:|
| apiUsername | Merchant Api Username gotten from merchant dashboard under credentials section | yes |
| apiPassword | Merchant Api Password  gotten from merchant dashboard under credentials section | yes |
| api_key | Merchant Api Key gotten from merchant dashboard under credentials section | yes |
| mode | The current mode operation. Can either be "**test**" when testing in sandbox or "**live**" when ready for production. | yes |
| return_url | The url or endpoint to be called upon payment completion | yes |
| notify_url | The url or endpoint to submit a transaction response to.This url or endpoint accepts a **POST** request of format:<br> <br> { <br> "transaction_id:"6465464", <br>"transaction_amount":"5000",<br>"transaction_status":"SUCCESS",<br>"error":null,<br>"message":"Transaction of 5000 XAF was successfully completed"<br>} | no |
| purchaseRef | A reference which you can give from your end to a particular transaction | no |
| total_amount | The amount or price of the product/products to be paid for. | yes |
| description | A description you can give to this type of  transaction | no |
| name | Name of the merchant | yes |
| currency | Can be XAF, USD or any currently supported currency | yes |
| transaction_id | *id that uniquely identifies the transaction and I must be unique for a transaction. This id should be alpha numeric and less than 20 characters* | yes
|

<br>

![](https://payunit.net/wp-content/uploads/2021/04/Screenshot-from-2021-04-21-17-37-05-2.png)

<br>

### Recommendations

For security concerns, make sure you read your Api key, Api password and Api user from a config file.

Consider saving your API credentials in environment variables for an extra layer of security. You can find a Flask demo app using payunit on github here

```
https://gitlab.com/sevencommonfactor/payunit-python_demo.git
```