# Flutter SDK
<br>

## Requirements

If you don't know how to get your API credentials in the Dashboard, please consult [Getting your API Key](https://payunit.net/docs/how-to-intall-2/#6-toc-title) documentation.

To Get Started with this SDK, Make sure you have a Flutter application ready. Also make sure you subscribe to the various payment providers you want

<br>

## Installation

```flutter
dependencies:

pay_unit_sdk: ^2.0.20
```
<br>

##### Use flutter sdk v.1.22.0 + 

##### Android minSdkVersion must be 19+ 

##### **Add mavenCentral() to your project :** 
Add mavenCentral() to allprojects in gradle > build.gradle .
<br>

```flutter
allprojects {

 repositories {

 google()

 mavenCentral() <---

 jcenter()

 }

}
```
​

Get all your packages Using:
```
flutter pub get
```
<br>

## Using the SDK

Import PayUnit in the required widget

<br>

```json
import 'package:pay_unit_sdk/payunitpackage.dart';

Add // @dart=2.9 to your main.dart before importation list to avoid null-safety

Use the pay button as a widget.

// @dart=2.9 <-- put this to your main.dart before importation list to avoid null-safety

import ....

 .

 .

 .

 .

 .

 PayUnitButton(

 apiUser:"<Your apiuser>",

 apiPassword:  "<Your apiPassword>",

 apiKey: "<Your apiKey>",

 transactionId: "<The id of your transaction>",

 mode: 'sandbox' // sandbox or live,

 transactionCallBackUrl:"<Your transactionCallBackUrl url>",

 notiFyUrl: "<Your notification url>",

 transactionAmount:  "<Your transaction amount>",

 currency:"XAF", //// The currency of your transaction : XAF for FCFA or USD for $ ect ...

 buttonTextColor:  "<Custom the color of the text PayUnit button>",

 productName:"<The name of the product>",

 color: "<Custom the color of PayUnit button>",///the colors of the PayUnit Button text DEFAULT WHITE,

 actionAfterProccess: (transactionId, transactionStatus) {

 //here is the action who start after the end of the paiement , you can perform

 //some operation here , like display a alertDialog after the end of the payment.

 },

 ),
```
<br>

Then in your view a PayUnit button will appear. When clicked, performs the actual payment.

### Demo

Watch demo video here 
<br>


[![Watch Video here](https://i.ytimg.com/vi_webp/QrcgV2g8LzE/maxresdefault.webp)](https://www.youtube.com/watch?v=QrcgV2g8LzE "Watch demo video here")
