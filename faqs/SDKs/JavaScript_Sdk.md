# Javascript SDK
<br>

### Pre-requisites

-   Create an account on Payunit as a merchant. Click [to create an account](https://app.payunit.net/#/).
-   Get the merchant Api User, merchant API key, and merchant API Password from the merchant dashboard under the credentials section.

<br>

 ![](https://payunit.net/wp-content/uploads/2021/04/Untitled-2-2.jpg)

<br>

## Installation and Usage of the SDK

There are two ways by which the SDK can be used.

-   Using the Payunit npm package or
-   Using the Payunit JavaScript CDN

**Demo**

You can find a Angular demo app using payunit on :

```
https://gitlab.com/sevencommonfactor/payunit-angular-demo/-/tree/dev
```

[![Watch demo video here](https://i.ytimg.com/vi_webp/67HDSwyiy0o/sddefault.webp)](https://youtu.be/67HDSwyiy0o "Watch demo video here")



<br>

Configuration [#](https://payunit.net/docs/javascript-sdk/#9-toc-title)
-----------------------------------------------------------------------

-   To Test Visa/Master Card in the Sandbox environment use the following information :
    -   Card Number: **4242 4224 2424 2424** or **2223 0000 4840 0011**

-   To test PayPal in the Sandbox environment use the following credential :
    -   Email: **sb-hf17g4673731@business.example.com**
    -   password**: ehQ5_)dA**

<br>

| Attribute | Description | Mandatory |
|:-------| :--------- | ---------:|
| apiUsername | Merchant Api Username gotten from merchant dashboard under credentials section | yes |
| apiPassword | Merchant Api Password  gotten from merchant dashboard under credentials section | yes |
| api_key | Merchant Api Key gotten from merchant dashboard under credentials section | yes |
| mode | The current mode operation. Can either be "**test**" when testing in sandbox or "**live**" when ready for production. | yes |
| return_url | The url or endpoint to be called upon payment completion | yes |
| notify_url | The url or endpoint to submit a transaction response to.This url or endpoint accepts a **POST** request of format:<br> <br> { <br> "transaction_id:"6465464", <br>"transaction_amount":"5000",<br>"transaction_status":"SUCCESS",<br>"error":null,<br>"message":"Transaction of 5000 XAF was successfully completed"<br>} | no |
| purchaseRef | A reference which you can give from your end to a particular transaction | no |
| total_amount | The amount or price of the product/products to be paid for. | yes |
| description | A description you can give to this type of  transaction | no |
| name | Name of the merchant | yes |
| currency | Can be XAF, USD or any currently supported currency | yes |
| transaction_id | *id that uniquely identifies the transaction and I must be unique for a transaction. This id should be alpha numeric and less than 20 characters*. An example can be of this form 465sdfsdf464asdfsa | yes
|

<br>

#### **2\. Using the SDK from its official CDN**

-   Add a script tag and set the value of the type attribute to module i.e type = "module".
-   Import the Payunit function from the CDN. In the snippet below, version 1.0.4 of the sdk is used which is currently the latest stable release.
-   Within the script tag, call the function as shown below and make sure you pass in all the required parameters.

Usage [#](https://payunit.net/docs/javascript-sdk/#11-toc-title)
----------------------------------------------------------------

```javascript
<script type="module">

 import { PayUnit } from "https://cdn.jsdelivr.net/npm/payunitjs@1.0.4/dist/payunit.min.js";

 PayUnit(

 {

 apiUsername: "",

 apiPassword: "",

 x_api_key: "",

 mode: "",

 },

 {

 return_url: " ",

 notify_url: " ",

 description: "",

 purchaseRef: "",

 total_amount: "",

 name: "",

 currency: "XAF",

 transaction_id: '',

 }

 );

</script>

<button id = "payunit-pay"> Pay </button>
```
<br>

![](https://payunit.net/wp-content/uploads/2021/04/newImg.png)

With everything in place, the payment is then processed and transaction result is submitted to the notify_url that was specified. In case the notify_url was not specified, the transaction result is sent as encoded query parameters to the return_url.

<br>

## Recommendations

For security concerns, make sure you read your Api key, Api password and Api user from a config file.